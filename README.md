# Projekt

Mateusz Borkowski, Paweł Popiołek

## Wstęp

Do wykonania zadania projektowego bazowano na aplikacji Pythonowej, na które bazowaliśmy wcześniej w ramach laboratoriów. Dla wykonania postawionego zadania wykorzystano pewną wariację aplikacji z laboratorium z dodanymi testami jednostkowymi dla klasy Book oraz wyeliminowaną podatnością XSS w tej klasie (poprzez weryfikacje danych wejściowych).

## Zadanie 1

Celem pierwszego zadania była bezpieczna konfiguracja repozytorium oraz realizacja procesu CI/CD zgodnie z kryteriami przedstawionymi w instrukcji.

W ramach realizacji zadania zagwarantowano, że Pull Requesty / Merge Requesty lub bezpośrednie pushe do repozytorium może wykonywać wyłącznie właściciel repozytorium:
![](/img/branch_settings.png)

Zgodnie z globalnymi ustawieniami dla GitLab nowe gałęzie może tworzyć dowolny użytkownik z rolą Developer lub wyższą. 

Wizualnie pipeline CI/CD prezentuje się następująco (screeny dla wersji która przeszła wszystkie testy):

- branch `main`
![](/img/cicd_viz_main.png)
  
- inny branch
![](/img/cicd_viz_beta.png)

Niepowodzenie któregoś ze wcześniejszych kroków uniemożliwy skuteczny release.

Opis (po kolei) zaprojektowanego procesu CI/CD (dostępny w pliku `.gitlab-ci.yml`):

Konfiguracja `default` określa domyślne ustawienia dla wszystkich etapów pipeline CI/CD. Używa obrazu `docker:20.10.16` jako podstawowego środowiska dla wszystkich zadań. Dodatkowo, definiuje usługę Docker-in-Docker (`docker:20.10.16-dind`), umożliwiając budowanie i uruchamianie kontenerów Dockera wewnątrz głównego kontenera CI (co przyda się do późniejszych testów):
```yml
default:
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
```

Kolejne uruchamiane etapy:
```yml
stages:
  - unit-testing
  - security-testing-static
  - build
  - security-testing-dynamic
  - release
```

`variables` definiuje zmienne środowiskowe wykorzystywane przez wszystkie etapy pipeline. `DOCKER_HOST` i `DOCKER_TLS_CERTDIR` konfigurują połączenie z usługą Docker-in-Docker. `CONTAINER_TEST_IMAGE` określa obraz używany do testów, bazując na rejestru CI i identyfikatorze commita. Zmienne `CONTAINER_RELEASE_IMAGE` i `CONTAINER_RELEASE_IMAGE_BETA` służą do definiowania nazw obrazów dla oficjalnych wydań i wersji beta, odpowiednio.

```yml
variables:
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  CONTAINER_RELEASE_IMAGE: $CI_DOCKER_HUB_REGISTRY_IMAGE:latest
  CONTAINER_RELEASE_IMAGE_BETA: $CI_DOCKER_HUB_REGISTRY_IMAGE:beta
```

Job **test-books** odpowiada za przeprowadzenie testów jednostkowych naszego kodu. Wykorzystuje obraz Pythona 3.9 z Alpine Linux do zainstalowania zależności zdefiniowanych w **requirements.txt** oraz uruchomienia testów jednostkowych w katalogu **project/books** za pomocą wbudowanego modułu **unittest**:
```yml
test-books:
  stage: unit-testing
  image:
    name: "python:3.9-alpine"
    entrypoint: [""]
  script:
    - pip install -r requirements.txt
    - python -m unittest discover -s project/books
```

W ramach własnej inicjatywy, zespół zdecydował się na realizację sprawdzenia narzędziem `gitleaks`, ze względu na częste pozotawianie sekretów w historii commitów w repozytoriach kodu oraz potencjalnie wysoką szkodliwość takich niedopatrzeń. W jobie `gitleaks-test` wykonywane jest statyczne testowanie bezpieczeństwa kodu źródłowego pod kątem potencjalnych wycieków danych wrażliwych. Używa najnowszego obrazu `gitleaks` do przeskanowania bieżącego katalogu roboczego w poszukiwaniu niezabezpieczonych sekretów i danych poufnych, co pomaga zapobiegać ich przypadkowemu wyciekowi:
```yml
gitleaks-test:
  stage: security-testing-static
  image:
    name: "zricethezav/gitleaks:latest"
    entrypoint: [""]
  script:
    - gitleaks detect --source="$(pwd)" -v
```

Job `dependency-check`(**SCA**) służy do identyfikacji znanych podatności w zależnościach projektu. Wykorzystuje obraz `owasp/dependency-check` do przeskanowania projektu i wygenerowania raportu o znalezionych zagrożeniach w bibliotekach, od których projekt jest zależny:
```yml
dependency-check:
  stage: security-testing-static
  image:
    name: owasp/dependency-check
  script:
    - dependency-check --scan .
```

Job `bandit-test` (**SAST**) jest przeznaczony do przeprowadzania statycznej analizy bezpieczeństwa kodu Pythona w poszukiwaniu powszechnych słabości i zagrożeń. W tym celu używany jest obraz `python:3.9-alpine`, na którym instalowany jest Bandit, narzędzie do analizy bezpieczeństwa, a następnie skanuje ono rekurencyjnie wszystkie pliki w katalogu `./project`:
```yml
bandit-test:
  stage: security-testing-static
  image:
    name: "python:3.9-alpine"
    entrypoint: [""]
  script:
    - pip install bandit
    - bandit -r ./project
```

Job `zap-test` (**DAST**) realizuje dynamiczne testy bezpieczeństwa aplikacji, symulując ataki na uruchomioną instancję aplikacji. W tym celu najpierw pobiera obraz testowy Docker, tworzy dedykowaną sieć `zapnet`, uruchamia kontener z aplikacją pod adresem `books.local` na porcie `5000`, a następnie używa narzędzia OWASP ZAP zawartego w obrazie `owasp/zap2docker-stable` do wykonania pełnego skanu aplikacji:
```yml
zap-test:
  stage: security-testing-dynamic
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker network create zapnet
    - docker run --network zapnet -d -h books.local -p 5000:5000 --name app-container $CONTAINER_TEST_IMAGE 
    - docker run --network zapnet -v $(pwd):/zap/wrk/:rw -t owasp/zap2docker-stable zap-full-scan.py -t http://books.local:5000
```

Job `build-image` odpowiada za budowanie obrazu Docker z kodu źródłowego aplikacji. Najpierw następuje uwierzytelnienie Dockera na Gitlab, a potem budowanie obrazu i otagowanie go jako `$CONTAINER_TEST_IMAGE`. Po zbudowaniu obraz jest wysyłany (`push`) do zdalnego rejestru Dockera:
```yml
build-image:
  stage: build
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build --pull -t $CONTAINER_TEST_IMAGE .
    - docker push $CONTAINER_TEST_IMAGE
```

Job `release-image-beta` jest odpowiedzialny za przygotowanie i wypuszczenie wersji beta obrazu Docker. Po uwierzytelnieniu w Dockerze na Gitlab (w repozytorium) oraz na Dockerhub, skrypt pobiera obraz testowy (z naszego repozytorium - zbudowany na etapie `buid`), otagowuje go jako wersję beta (`$CONTAINER_RELEASE_IMAGE_BETA`) i wypycha go na Dockerhuba. Jest to realizowane dla wszystkich gałęzi kodu źródłowego oprócz gałęzi głównej (`main`):
```yml
release-image-beta:
  stage: release
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker login -u $CI_REGISTRY_DOCKER_HUB_USER -p $CI_REGISTRY_DOCKER_HUB_PASSWORD $CI__DOCKER_HUB_REGISTRY
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE_BETA
    - docker push $CONTAINER_RELEASE_IMAGE_BETA
  except:
    - main
```

Job `release-image` zajmuje się finalnym wydaniem obrazu Docker dla głównej gałęzi kodu (`main`). Po podwójnym uwierzytelnieniu (Gitlab Docker oraz Dockerhub), proces polega na pobraniu obrazu testowego (z naszego repozytorium - zbudowany na etapie `buid`), otagowaniu go jako oficjalny obraz (`$CONTAINER_RELEASE_IMAGE`) i opublikowaniu go na Dockerhub. Ten etap jest wykonywany wyłącznie dla zmian w gałęzi `main`. **Ten krok gwarantuje, że obraz z tagiem latest budowany jest wyłącznie z gałęzi `main`** (instrukcja punkt 1.1.):
```yml
release-image:
  stage: release
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker login -u $CI_REGISTRY_DOCKER_HUB_USER -p $CI_REGISTRY_DOCKER_HUB_PASSWORD $CI__DOCKER_HUB_REGISTRY
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push $CONTAINER_RELEASE_IMAGE
  only:
    - main
```

Dla naszej wersji aplikacji test `DAST` (`ZAP`) zwracał warningi dotyczące znalezionych przez narzędzie problemów, jeszcze zanim wprowadziliśmy do aplikacji własne podatności. Na potrzebny przetestowania skuteczności wdrożenia na `Dockerhub`, włączyliśmy ignorowanie warningów w `ZAP`. Dzięki temu, takie obrazy zostały opublikowane:

![](/img/dockerhub.png)

Link do `Dockerhub`: https://hub.docker.com/repository/docker/ferruleguiders0t/cicd_proj/general


## Zadanie 2

W celu pełnej weryfikacji efektywności testów bezpieczeństwa zaimplementowanych w procesie CI/CD, przeprowadzono testy mające na celu sprawdzenie zdolności systemu do wykrywania dwóch kluczowych podatności bezpieczeństwa: możliwości wstrzyknięcia skryptów JavaScript (Cross-Site Scripting) oraz pobierania lokalnych plików z serwera (Local File Inclusion).

_[Gałąź na której realizowano testy w ramach zad 2](https://gitlab.com/ppopiolek1/cicd_proj/-/tree/zadanie2?ref_type=heads)_

_[Link do pipeline z wprowadzenia podatności](https://gitlab.com/ppopiolek1/cicd_proj/-/pipelines/1151627403)_ (zatrzymanie na testach jednostkowych dla XSS)

_[Link do pipeline z ograniczonymi testami jednostkowymi](https://gitlab.com/ppopiolek1/cicd_proj/-/pipelines/1151649366)_ - dla sprawdzenia reszty job'ów (Wykrycie podatności LFI przez ZAP)

### Podatność XSS

Drugim testem mającym na celu weryfikację działania testów bezpieczeństwa w procesie CI/CD było stworzenie aplikacji zawierającej podatność XSS. W tym celu z aplikacji wyeliminowano weryfikację danych wejściowych dla klasy Books.

<details><summary>Zrzuty ekranu potwierdzające możliwość realizacji podatności</summary>

![](/img/books_xss_payload.png)

![](/img/books_xss_name.png)

![](/img/books_xss_author.png)

</details>
 
<details><summary>Następnie na takiej nowej gałęzi wywołano proces CICD, który przerwał swoje wykonanie na etapie testów jednostkowych (co jest spodziewanym zachowaniem)</summary>

pipeline: https://gitlab.com/ppopiolek1/cicd_proj/-/pipelines/1151627403

```
$ python -m unittest discover -s project/books
77FFFFF.F
78======================================================================
79FAIL: test_extreme_data_int (test.TestBook)
80----------------------------------------------------------------------
81Traceback (most recent call last):
82  File "/builds/ppopiolek1/cicd_proj/project/books/test.py", line 101, in test_extreme_data_int
83    Book(
84AssertionError: ValueError not raised
85======================================================================
86FAIL: test_extreme_data_string (test.TestBook)
87----------------------------------------------------------------------
88Traceback (most recent call last):
89  File "/builds/ppopiolek1/cicd_proj/project/books/test.py", line 83, in test_extreme_data_string
90    Book(
91AssertionError: ValueError not raised
92======================================================================
93FAIL: test_invalid_data_length (test.TestBook)
94----------------------------------------------------------------------
95Traceback (most recent call last):
96  File "/builds/ppopiolek1/cicd_proj/project/books/test.py", line 38, in test_invalid_data_length
97    Book(
98AssertionError: ValueError not raised
99======================================================================
100FAIL: test_invalid_data_type (test.TestBook)
101----------------------------------------------------------------------
102Traceback (most recent call last):
103  File "/builds/ppopiolek1/cicd_proj/project/books/test.py", line 28, in test_invalid_data_type
104    Book(
105AssertionError: ValueError not raised
106======================================================================
107FAIL: test_sql_injections (test.TestBook)
108----------------------------------------------------------------------
109Traceback (most recent call last):
110  File "/builds/ppopiolek1/cicd_proj/project/books/test.py", line 50, in test_sql_injections
111    Book(
112AssertionError: ValueError not raised
113======================================================================
114FAIL: test_xss_injections (test.TestBook)
115----------------------------------------------------------------------
116Traceback (most recent call last):
117  File "/builds/ppopiolek1/cicd_proj/project/books/test.py", line 66, in test_xss_injections
118    Book(
119AssertionError: ValueError not raised
120----------------------------------------------------------------------
121Ran 7 tests in 0.040s
122FAILED (failures=6)
123/builds/ppopiolek1/cicd_proj/uploads/

124
Cleaning up project directory and file based variables
00:00
125ERROR: Job failed: exit code 1
```
</details>

Dla dodatkowego sprawdzenia reszty procesu, a nie przerywania pipeline na samym początku, możliwe ograniczono testy jednostkowe, tak aby przeszły bez problemu i spowodowały wywołanie późniejszych jobów. 
Niestety, mimo przeprowadzenia zaawansowanych testów bezpieczeństwa, w tym z użyciem narzędzia do dynamicznych testów aplikacji narzędziem ZAP, żadne z tych rozwiązań nie wykryło istnienia podatności XSS w aplikacji. To wynik, który wymaga dodatkowej analizy w celu zidentyfikowania przyczyn braku wykrycia przez zaimplementowane narzędzia bezpieczeństwa. Link do pipeline z ograniczonymi testami jednostkowymi: https://gitlab.com/ppopiolek1/cicd_proj/-/pipelines/1151649366.

### Podatność Local File Inclusion

W celu symulacji potencjalnego ataku, na gałęzi o nazwie "zadanie2" została umyślnie dodana podatność bezpieczeństwa związana z Local File Inclusion. Aplikacja została skonfigurowana do wyświetlania regulaminu biblioteki, który był umieszczony w postaci pliku PDF na serwerze z aplikacją. Implementacja dołączania plików z folderu uploads została wykonana niepoprawnie w związku z czym pojawiła się możliwość dołączania dowolnych plików z serwera. 

![](/img/lfi_5.png)

<details><summary>zrzuty ekranu z wykorzystania podatności</summary>

![](/img/lfi_1.png)

![](/img/lfi_2.png)

![](/img/lfi_4.png)

![](/img/lfi_3.png)

![](/img/lfi_6.png)

</details>

W ramach procesu CI/CD, jedynie testy dynamiczne, przeprowadzane przy użyciu narzędzia ZAP, ujawniły obecność umyślnie wprowadzonej podatności związanej z LFI. Poniżej przedstawione są nowe wyniki działania narzędzia ZAP, które nie występowały przed wprowadzeniem podatności.

```
WARN-NEW: Buffer Overflow [30001] x 1 
	http://books.local:5000/file?name=regulamin.pdf (500 INTERNAL SERVER ERROR)
WARN-NEW: Integer Overflow Error [30003] x 1 
	http://books.local:5000/file?name=regulamin.pdf (500 INTERNAL SERVER ERROR)
WARN-NEW: Path Traversal [6] x 1 
	http://books.local:5000/file?name=%2Fetc%2Fpasswd (200 OK)
```





